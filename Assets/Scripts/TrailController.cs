using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailController : MonoBehaviour
{
    [SerializeField] private GameObject trail;
    
    private Bird m_targetBird;
    private List<GameObject> m_trails;

    private void Start()
    {
        m_trails = new List<GameObject>();
    }

    public void SetBird(Bird bird)
    {
        m_targetBird = bird;
        foreach (var tr in m_trails)
        {
            Destroy(tr.gameObject);
        }

        m_trails.Clear();
    }

    public IEnumerator SpawnTrail()
    {
        m_trails.Add(Instantiate(trail, m_targetBird.transform.position, Quaternion.identity));
        yield return new WaitForSeconds(.1f);

        if (m_targetBird != null && m_targetBird.State != Bird.BirdState.HitSomething)
        {
            StartCoroutine(SpawnTrail());
        }
    }
}
