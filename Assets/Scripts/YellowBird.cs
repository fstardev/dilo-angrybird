using UnityEngine;

public class YellowBird : Bird
{
     [SerializeField] private float boostForce = 100f;
     [SerializeField] private bool hasBoost;

     
     private void Boost()
     {
          if (State != BirdState.Thrown || hasBoost) return;
          Rigidbody.AddForce(Rigidbody.velocity * boostForce);
          hasBoost = true;
     }

     public override void OnTap()
     {
          Boost();
     }
}
