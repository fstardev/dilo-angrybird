using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Bird : MonoBehaviour
{
     public enum BirdState
     {
          Idle,
          Thrown,
          HitSomething,
     }

     [SerializeField] private ParticleSystem deathFx;
     [SerializeField] private GameObject birdImage;
     protected Rigidbody2D Rigidbody;
     
     private CircleCollider2D m_collider;
     private const float MINVelocity = .05f;

     public UnityAction OnBirdDestroyed = delegate {  };
     public Action<Bird> OnBirdShot = delegate {  };
     
     public BirdState State { get; private set; }

     public bool FlagDestroy { get; private set; }

     private void Awake()
     {
          Rigidbody = GetComponent<Rigidbody2D>();
          m_collider = GetComponent<CircleCollider2D>();
     }

     private void Start()
     {
          Rigidbody.bodyType = RigidbodyType2D.Kinematic;
          m_collider.enabled = false;
          State = BirdState.Idle;
     }

     private void FixedUpdate()
     {
          if (State == BirdState.Idle && Rigidbody.velocity.sqrMagnitude >= MINVelocity)
          {
               State = BirdState.Thrown;
          }

          if ((State != BirdState.Thrown && State != BirdState.HitSomething) ||
              !(Rigidbody.velocity.sqrMagnitude < MINVelocity) || FlagDestroy) return;
          FlagDestroy = true;
          StartCoroutine(DestroyAfter(1));

     }

     protected void OnDestroy()
     {
          if (State == BirdState.Thrown || State == BirdState.HitSomething)
          {
               OnBirdDestroyed();
          }
     }

     private void OnCollisionEnter2D()
     {
          State = BirdState.HitSomething;
     }
     

     private IEnumerator DestroyAfter(float seconds)
     {
          yield return new WaitForSeconds(seconds);
          Rigidbody.freezeRotation = true;
          DestroyBirdFX();
          yield return new WaitForSeconds(seconds);
          Destroy(gameObject);
     }

     protected virtual void DestroyBirdFX()
     {
          birdImage.SetActive(false);
          deathFx.Play();
     }

     public void MoveTo(Vector2 target, GameObject parent)
     {
          var o = gameObject;
          o.transform.SetParent(parent.transform);
          o.transform.position = target;
     }

     public void Shoot(Vector2 velocity, float distance, float speed)
     {
          if (m_collider == null) return;
          m_collider.enabled = true;
          Rigidbody.bodyType = RigidbodyType2D.Dynamic;
          Rigidbody.velocity = velocity * speed * distance;
          OnBirdShot(this);
     }

     public virtual void OnTap(){}
}
