using UnityEngine;

public class Destroyer : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        var targetTag = other.gameObject.tag;
        if (targetTag == "Bird" || targetTag == "Enemy" || targetTag == "Obstacle")
        {
            Destroy(other.gameObject, 1);
        }
      
    }
}
