using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField] private float health = 30;
    [SerializeField] private ParticleSystem breakFX;

    private void OnCollisionEnter2D(Collision2D other)
    {
       
        var targetTag = other.gameObject.tag;
        if (targetTag != "Bird" && targetTag != "Enemy" && targetTag != "Obstacle") return;
        var damage = other.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 10;
        health -= damage;

        if (!(health <= 0)) return;
        var fx = Instantiate(breakFX, transform.position, Quaternion.identity);
        Destroy(fx, 2);
        Destroy(gameObject);

    }
}
