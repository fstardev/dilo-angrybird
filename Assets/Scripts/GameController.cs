using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private SlingShooter slingShooter;
    [SerializeField] private List<Bird> birds;
    [SerializeField] private List<Enemy> enemies;
    [SerializeField] private TrailController trailController;
   
    private BoxCollider2D m_tapCollider;
    private bool m_isGameEnded;
    private Bird m_shootBird;


    private void Awake()
    {
        m_tapCollider = GetComponent<BoxCollider2D>();
    }

    private void Start()
    {
        foreach (var bird in birds)
        {
            bird.OnBirdDestroyed += ChangeBird;
            bird.OnBirdShot += AssignTrail;
        }

        foreach (var enemy in enemies)
        {
            enemy.OnEnemyDestroyed += CheckGameEnd;
        }

        m_tapCollider.enabled = false;
        slingShooter.InitiateBird(birds[0]);
        m_shootBird = birds[0];
    }

    private void ChangeBird()
    {
        if (m_tapCollider == null) return;
        m_tapCollider.enabled = false;
        if (m_isGameEnded) return;
        birds.RemoveAt(0);
        if (birds.Count > 0)
        {
            if (slingShooter == null) return;
            slingShooter.InitiateBird(birds[0]);
            m_shootBird = birds[0];
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
       
    }

    private void AssignTrail(Bird bird)
    {
        trailController.SetBird(bird);
        StartCoroutine(trailController.SpawnTrail());
        m_tapCollider.enabled = true;
    }

    private void OnMouseUp()
    {
        if (m_shootBird != null)
        {
            m_shootBird.OnTap();
        }
    }

    private void CheckGameEnd(GameObject destroyedEnemy)
    {
        for (var i = 0; i < enemies.Count; i++)
        {
            if(enemies[i] == null) continue;
            if (enemies[i].gameObject != destroyedEnemy) continue;
            enemies.RemoveAt(i);
            break;
        }

        if (enemies.Count != 0) return;
        m_isGameEnded = true;
        var currentScene = SceneManager.GetActiveScene().buildIndex;
        if (currentScene == 0)
        {
            SceneManager.LoadScene(++currentScene);
        }
    }
}
