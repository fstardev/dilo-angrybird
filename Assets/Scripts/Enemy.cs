using System;
using UnityEngine;
using UnityEngine.Events;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float health = 50f;
    [SerializeField] private ParticleSystem deathFx;
    [SerializeField] private GameObject pigImage;
    
    public UnityAction<GameObject> OnEnemyDestroyed = delegate {  };

    private bool m_isHit;
    private Rigidbody2D m_rigidbody;

    private void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
    }

    private void OnDestroy()
    {
        if (m_isHit)
        {
            OnEnemyDestroyed(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.GetComponent<Rigidbody2D>() == null) return;
        if (other.gameObject.CompareTag("Bird"))
        {
            var bird = other.gameObject.GetComponent<Bird>();
            if (bird.FlagDestroy) return;
            m_isHit = true;
            DestroyPigFX();

        }
        else if (other.gameObject.CompareTag("Obstacle"))
        {
            var damage = other.gameObject.GetComponent<Rigidbody2D>().velocity.magnitude * 10;
            health -= damage;

            if (!(health <= 0)) return;
            m_isHit = true;
            DestroyPigFX();
        }
    }

    private void DestroyPigFX()
    {
        m_rigidbody.freezeRotation = true;
        deathFx.Play();
        pigImage.SetActive(false);
        Destroy(gameObject, 1);
    }

    public void SelfDestruct()
    {
        health = 0;
        m_isHit = true;
        DestroyPigFX();
    }
}
