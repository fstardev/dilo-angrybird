using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class OwlBird : Bird
{
    [SerializeField] private ParticleSystem explosionFx;
    [SerializeField] private float explosionRadius;
    [SerializeField] private float explosionForce;
    [SerializeField] private LayerMask explosionLayer;

    protected override void DestroyBirdFX()
    {
        base.DestroyBirdFX();
        Explode();
    }

    private void Explode()
    {
        var overlapObjects = new Collider2D[5];
        if (Physics2D.OverlapCircleNonAlloc(transform.position, explosionRadius, overlapObjects, explosionLayer) > 0)
        {
            foreach (var overlapObject in overlapObjects.Where(obj => obj != null))
            {
                var direction = overlapObject.transform.position - transform.position;
                overlapObject.GetComponent<Rigidbody2D>().AddForce(direction * explosionForce);
                if (!overlapObject.CompareTag("Enemy")) continue;
                var pig = overlapObject.GetComponent<Enemy>();
                pig.SelfDestruct();
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
