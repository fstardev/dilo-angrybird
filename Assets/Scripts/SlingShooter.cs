using System;
using UnityEngine;


public class SlingShooter : MonoBehaviour
{
  
   [SerializeField] private float radius = .75f;
   [SerializeField] private float throwSpeed = 30f;
   [SerializeField] private LineRenderer trajectory;
   
   private CircleCollider2D m_coll;
   private Vector2 m_startPost;
   private Bird m_bird;

   private void Awake()
   {
      m_coll = GetComponent<CircleCollider2D>();
   }

   private void Start()
   {
      m_startPost = transform.position;
   }

   private void OnMouseUp()
   {
      m_coll.enabled = false;
      var position = transform.position;
      var velocity = m_startPost - (Vector2)position;
      var distance = Vector2.Distance(m_startPost, position);
      m_bird.Shoot(velocity, distance, throwSpeed);
      gameObject.transform.position = m_startPost;

      trajectory.enabled = false;
   }

   private void OnMouseDrag()
   {
      if (Camera.main is null) return;
      Vector2 p = Camera.main.ScreenToWorldPoint(Input.mousePosition);
      var dir = p - m_startPost;
      if (dir.sqrMagnitude > radius)
      {
         dir = dir.normalized * radius;
      }

      var trans = transform;
      trans.position = m_startPost + dir;

      var distance = Vector2.Distance(m_startPost, trans.position);

      if (!trajectory.enabled)
      {
         trajectory.enabled = true;
      }

      DisplayTrajectory(distance);

   }

   private void DisplayTrajectory(float distance)
   {
      if (m_bird == null) return;

      var position = transform.position;
      var velocity = m_startPost - (Vector2)position;
      const int segmentCount = 5;
      var segments = new Vector2[segmentCount];

      segments[0] = position;
      var segVelocity = velocity * throwSpeed * distance;

      for (var i = 0; i < segmentCount; i++)
      {
         var elapsedTime = i * Time.fixedDeltaTime * 5;
         segments[i] = segments[0] + segVelocity * elapsedTime + .5f * Physics2D.gravity * Mathf.Pow(elapsedTime, 2);
      }

      trajectory.positionCount = segmentCount;
      for (var i = 0; i < segmentCount; i++)
      {
         trajectory.SetPosition(i, segments[i]);
      }
   }

   public void InitiateBird(Bird bird)
   {
      m_bird = bird;
      var o = gameObject;
      m_bird.MoveTo(o.transform.position, o);
      m_coll.enabled = true;
   }
}
